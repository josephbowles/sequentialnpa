from setuptools import setup

setup(name='sequentialnpa',
      version='0.2',
      description='NPA Hierarchy for sequential correlations',
      url='https://gitlab.com/josephbowles/sequentialnpa',
      author='Joseph Bowles',
      author_email='bowles.physics@gmail.com',
      license='',
      packages=['sequentialnpa'],
      zip_safe=False)
