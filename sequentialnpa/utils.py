from itertools import product
from ncpol2sdpa.nc_utils import get_all_monomials, apply_substitutions, HermitianOperator, flatten, generate_operators
from sympy import S

def generate_measurements(party, label):
    measurements = []
    for i in range(len(party)):
        measurements.append(generate_operators(label + '%s' % i, party[i],
                                               hermitian=True))
    return measurements

def generate_operators_12(name, n_vars, hermitian=None, commutative=False):
    variables = [[0 for __ in range(n_vars[1])] for __ in range(n_vars[0])]
    for i in range(n_vars[0]):
        for j in range(n_vars[1]):
            var_name = '%s_%s%s' % (name, i,j)
            variables[i][j]=(HermitianOperator(var_name))
    return variables

def generate_measurements_12(party, label):
    measurements = [[0 for __ in range(len(party[1]))] for __ in range(len(party[0]))]
    for i in range(len(party[0])):
        for j in range(len(party[1])):
            measurements[i][j]=(generate_operators_12(label + '%s%s' % (i,j), [party[0][i],party[1][j]],
                                                   hermitian=True))
    return measurements

def gen_reduced_meas_12(ny1,ny2,nb1,nb2,b_ops):
    b_red = [[] for __ in range(ny2) for __ in range(ny1)]
    c = 0
    for y1, y2 in product(range(ny1), range(ny2)):
        for b1, b2 in product(range(nb1), range(nb2)):
            if y2 != ny2 - 1 and b2 != nb2 - 1:
                b_red[c].append(b_ops[y1][y2][b1][b2])
            if y2 == ny2 - 1 and not (b1 == nb1 - 1 and b2 == nb2 - 1):
                b_red[c].append(b_ops[y1][y2][b1][b2])
        c += 1

    return b_red

def get_orthog_subs(ny1,ny2,nb1,nb2,b_ops):
    orthog_subs={}
    for y1, y2, yy2, b1, b2, bb1, bb2 in product(range(ny1),
                                                 range(ny2),
                                                 range(ny2),
                                                 range(nb1),
                                                 range(nb2),
                                                 range(nb1),
                                                 range(nb2)):
        if y2 != yy2 and b1 != bb1:
            orthog_subs[b_ops[y1][y2][b1][b2] * b_ops[y1][yy2][bb1][bb2]] = 0

    return orthog_subs

def get_local_subs(a_red,b_red):
    local_subs={}
    for party in [flatten(a_red), flatten(b_red)]:
        for i in range(len(party)):
            for j in range(i + 1, len(party)):
                local_subs[party[i] * party[j]] = party[j] * party[i]
    return local_subs

def get_norm_subs_12(ny1,ny2,nb1,nb2,b_ops):
    norm_subs_12={}
    for y1 in range(ny1):
        for y2 in range(ny2):
            tempsum = 0
            for b1 in range(nb1):
                for b2 in range(nb2):
                    if b1 != nb1 - 1 or b2 != nb2 - 1:
                        tempsum = tempsum + b_ops[y1][y2][b1][b2]
            norm_subs_12[b_ops[y1][y2][nb1 - 1][nb2 - 1]] = (S.One - tempsum).expand()
    return norm_subs_12

def get_norm_subs(nx,na,a_ops):
    norm_subs_1={}
    for x in range(nx):
        tempsum = 0
        for a in range(na - 1):
            tempsum += a_ops[x][a]
        norm_subs_1[a_ops[x][na - 1]] = S.One - tempsum
    return norm_subs_1

def get_seq_subs(ny1,ny2,nb1,nb2,b_ops):
    seq_subs={}
    for y1, b1 in product(range(ny1), range(nb1)):
        G = sum(b_ops[y1][ny2 - 1][b1][b2] for b2 in range(nb2))
        for y2 in range(ny2 - 1):
            seq_subs[b_ops[y1][y2][b1][nb2 - 1]] = G - sum(b_ops[y1][y2][b1][b2] for b2 in range(nb2 - 1))

    return seq_subs


def get_seq_constraints(ny1,ny2,nb1,nb2,b_ops,b_red,a_red,subs,norm_subs,seq_subs,level,doAB):
    # Define no signalling conditions as momentequalities
    nosig = []
    seq_constraints = []

    # These are the constraints that arise from eliminating some of Bob's operators
    # due to NS constraints from B2 to B1 (i.e. the same as the subs above)
    for y1, y2, b1, b2 in product(range(ny1), range(ny2), range(nb1), range(nb2)):
        if not (b_ops[y1][y2][b1][b2] in flatten(b_red)):
            # projective condition
            temp_op = apply_substitutions(
                b_ops[y1][y2][b1][b2] * b_ops[y1][y2][b1][b2] - b_ops[y1][y2][b1][b2],
                {**norm_subs, **seq_subs, **subs})
            if temp_op != 0 and not (temp_op in nosig):
                nosig.append(temp_op)
            # orthogonality of operators
            for bb1, bb2, yy2 in product(range(nb1), range(nb2), range(ny2)):
                if bb1 != b1:
                    tempop = apply_substitutions(
                        b_ops[y1][y2][b1][b2] * b_ops[y1][yy2][bb1][bb2],
                        {**norm_subs, **seq_subs, **subs})
                    if temp_op != 0 and not (temp_op in nosig):
                        nosig.append(temp_op)

    if level == 1 and not (doAB):
        L1 = [S.One]
    elif level == 1 and doAB:
        L1 = get_all_monomials(flatten(a_red), [], subs, 1)
    else:
        L1 = get_all_monomials(flatten([a_red, b_red]), [], subs, 1)

    # Generate many instances of polynoimals that should be equal to zero and
    # include them as moment equalities
    for nsop in nosig:
        for op in L1:
            k1 = apply_substitutions((nsop * op).expand(), subs)
            if k1 != 0 and not (k1 in seq_constraints):
                seq_constraints.append(k1)
            k2 = apply_substitutions((op * nsop).expand(), subs)
            if k2 != 0 and not (k2 in seq_constraints):
                seq_constraints.append(k2)

    return seq_constraints

def gen_ops_pguess(scenario,doAB):
    nx, ny1, ny2, na, nb1, nb2 = scenario

    bob = [[nb1 for __ in range(ny1)], [nb2 for __ in range(ny2)]]
    alice = [na for i in range(nx)]

    # we need to do this for each output of b1,b2 since we have a moment
    # matrix for each

    a_ops={}
    b_ops={}
    a_red={}
    b_red={}
    ab={}

    for i1 in range(nb1):
        for i2 in range(nb2):

            a_ops[i1, i2] = generate_measurements(alice, 'A{0}{1}_'.format(i1, i2))
            b_ops[i1, i2] = generate_measurements_12(bob, 'B{0}{1}_'.format(i1, i2))
            b_red[i1, i2] = []
            ab[i1, i2] = []

            for y1, y2 in product(range(ny1), range(ny2)):
                # if y1==ny2-1 we remove only the last operator
                if y2 == ny2 - 1:
                    b_red[i1, i2] += [flatten(b_ops[i1, i2][y1][y2])[:-1]]
                # otherwise, we remove all those with b2=nb2-1
                else:
                    temp_list = []
                    for b1, b2 in product(range(nb1), range(nb2)):
                        if b2 != nb2 - 1:
                            temp_list += [b_ops[i1, i2][y1][y2][b1][b2]]
                    b_red[i1, i2] += [temp_list]

            # usual removal of the last operator for Alice
            a_red[i1, i2] = [flatten(a_ops[i1, i2][x])[:-1] for x in range(nx)]

            # for a potential level 1+AB (currently unused)
            if doAB:
                for op1 in flatten(a_red[i1, i2]):
                    for op2 in flatten(b_red[i1, i2]):
                        ab[i1, i2].append(op1 * op2)

    return a_ops, b_ops, a_red, b_red, ab

def get_eq_constraints_pguess(p,a_ops,b_ops,nb1,nb2,L2):
    eq_constraints = []
    for key in p:
        temp_op = 0
        for i1 in range(nb1):
            for i2 in range(nb2):
                key_op = key.replace('A', 'a_ops[{0},{1}]'.format(i1, i2))
                key_op = key_op.replace('B', 'b_ops[{0},{1}]'.format(i1, i2))
                # only constrain those probabilities appearing in the moment matrix
                if eval(key_op) in L2[i1, i2]:
                    temp_op += eval(key_op)

        if eval(key_op) in L2[i1, i2]:
            eq_constraints += [temp_op - p[key]]

    return eq_constraints

def get_pguess(nb1,nb2,b_ops,input,seq_subs):
    gp = 0
    for i1 in range(nb1):
        for i2 in range(nb2):
            if i1 != (nb1 - 1) or i2 != (nb2 - 1):
                gp = gp + b_ops[i1, i2][input[0]][input[1]][i1][i2]
                gp = gp - b_ops[nb1 - 1, nb2 - 1][input[0]][input[1]][i1][i2]
    gp = apply_substitutions(gp, seq_subs)
    extraexp = "-" + str(nb1 * nb2 - 1) + "[0,0]"

    return gp, extraexp


