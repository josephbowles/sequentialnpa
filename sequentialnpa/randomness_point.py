import numpy as np
import math
import itertools as it

def p_flo(epsilon, theta=0, noise=0):

    nx=2
    na=2
    ny1=2
    nb1=2
    ny2=3
    nb2=3

    # tilted measurements
    mu = math.atan(math.sin(2 * epsilon))
    sz = np.array([[1, 0], [0, -1]])
    sx = np.array([[0, 1], [1, 0]])
    sy = np.array([[0, 1j], [-1j, 0]])

    op1 = math.cos(mu) * sz + math.sin(mu) * sx
    op2 = math.cos(mu) * sz - math.sin(mu) * sx
    e1 = np.linalg.eig(op1)[1][:, 0]
    e2 = np.linalg.eig(op2)[1][:, 0]

    # for the mercedes POVM
    v1 = [math.cos(0 + theta), math.sin(0 + theta)]
    v2 = [math.cos(2 * math.pi / 6 + theta), math.sin(2 * math.pi / 6 + theta)]
    v3 = [math.cos(4 * math.pi / 6 + theta), math.sin(4 * math.pi / 6 + theta)]

    # Alice meas ops
    A = {}
    A[0, 0] = np.outer(e1, e1)
    A[0, 1] = np.eye(2) - A[0, 0]
    A[1, 0] = np.outer(e2, e2)
    A[1, 1] = np.eye(2) - A[1, 0]

    # Kraus ops
    G = {}
    G[0, 0] = np.outer([1, 0], [1, 0])
    G[0, 1] = np.eye(2) - np.outer([1, 0], [1, 0])
    G[1, 0] = math.cos(epsilon) * np.outer([1, 1], [1, 1]) / 2 + math.sin(epsilon) * np.outer([1, -1], [1, -1]) / 2
    G[1, 1] = -math.cos(epsilon) * np.outer([1, -1], [1, -1]) / 2 + math.sin(epsilon) * np.outer([1, 1], [1, 1]) / 2

    # Bob's second meas ops
    M = {}
    M[0, 0] = np.outer([1, 0], [1, 0])
    M[0, 1] = np.eye(2) - M[0, 0]
    M[0, 2] = np.zeros(2)
    M[1, 0] = np.outer([1, 1], [1, 1]) / 2
    M[1, 1] = np.eye(2) - M[1, 0]
    M[1, 2] = np.zeros(2)

    M[2, 0] = np.outer(v1, v1) * 2 / 3
    M[2, 1] = np.outer(v2, v2) * 2 / 3
    M[2, 2] = np.outer(v3, v3) * 2 / 3

    B = {}
    for y1, y2, b1, b2 in it.product(range(ny1), range(ny2), range(nb1), range(nb2)):
        B[y1, y2, b1, b2] = np.matmul(np.matmul(G[y1, b1], M[y2, b2]), G[y1, b1])

    psi = (1 - noise) * np.outer([1, 0, 0, 1], [1, 0, 0, 1]) / 2 + noise * np.eye(4) / 4

    # get the p
    p = {}

    for x, a in it.product(range(nx), range(na)):
        p['A[{0}][{1}]'.format(x,a)] = np.trace(np.matmul(np.kron(A[x, a], np.eye(2)), psi))

    for y, b in it.product(range(ny1 * ny2), range(nb1 * nb2)):
        (y1, y2) = np.unravel_index(y, [ny1, ny2])
        (b1, b2) = np.unravel_index(b, [nb1, nb2])
        p['B[{0}][{1}][{2}][{3}]'.format(y1,y2,b1,b2)] = np.trace(np.matmul(np.kron(np.eye(2), B[y1, y2, b1, b2]), psi)).real
        for x, a in it.product(range(nx), range(na)):
            p['A[{0}][{1}]*B[{2}][{3}][{4}][{5}]'.format(x,a,y1,y2,b1,b2)] = np.trace(np.matmul(np.kron(A[x, a], B[y1, y2, b1, b2]), psi)).real

    return p