from .utils import *
from ncpol2sdpa import projective_measurement_constraints, SdpRelaxation
from ncpol2sdpa.nc_utils import apply_substitutions

def sequentialnpa(constraints,coeffs,alices,bobs,scenario,level,local=False, solver='mosek'):
    """
    Optimisation over relaxations of sets of sequential quantum correlations.
    Can fix elements of moment matrix to be constant, and/or minimise a linear objective

    :param constraints: dictionary containing constraints in string format.

        e.g. for scenario with 1 Alice and 2 Bobs
        constraints['A[x][a]*B[y1][y2][b1][b2]']=0.5 
        would indicate that the probability p(a,b1,b2|x,y1,y2)=0.5
    
        one can also constrain linear expressions. e.g. 
        constraints['A[0][0]*B[0][0][0][0]+2*A[1][0]*B[1][1][0][0]']=1.0
    
        One can also include elements of the moment matrix that do not correspond to 
        observable probabilities
        e.g. p['A[x1][a1]*A[x2][a2]']=0.2j
    
    :param coeffs: dictionary containing coefficients of a linear expression to minimise
    
        e.g. coeffs['A[0][0]*B[0][0][0][0]']=1.
             coeffs['A[1][0]*B[1][1][0][0]']=-1.
             
        minimises the expression A[0][0]*B[0][0][0][0] - A[1][0]*B[1][1][0][0]

    :param alices: int number of Alices

    :param: bobs: int number of Bobs

    :param scenario: list describing the measurement scenario

        format [nx,ny1,ny2,na,nb1,nb2] or [nx1,nx2,ny1,ny2,na1,na2,nb1,nb2]
        where nx, na is number of inputs/outputs for alice etc

    :param level: int specifying relaxation level

        for level 1+AB use level = 'AB'

    :param local: use the relaxation of the local polytope

    :param: solver: string specifying the sdp solver. set to mosek by default.

    :return: returns ncpol2sdpa.SdpRelaxation object (see ncpol2sdpa documentation)

    """

    if [alices,bobs]==[1,2]:
        sdp = q_seq_abb(constraints,coeffs,scenario,level,local=False, solver='mosek')
    if [alices,bobs]==[2,2]:
        sdp = q_seq_aabb(constraints,coeffs,scenario,level,local=False, solver='mosek')

    print('SDP status is '+sdp.status)
    print('primal value '+str(sdp.primal))
    print('dual value '+str(sdp.dual))

    return sdp

def q_seq_abb(constraints,coeffs,scenario,level,local=False, solver='mosek'):

    # Measurement scenario
    nx, ny1, ny2, na, nb1, nb2 = scenario

    if level=='AB':
        doAB=True
    else:
        doAB=False

    ###create operators
    alice = [na for __ in range(nx)]
    bob = [[nb1 for __ in range(ny1)], [nb2 for __ in range(ny2)]]

    #measurement operators
    a_ops = generate_measurements(alice, 'A_')
    b_ops = generate_measurements_12(bob, 'B_')

    #reduced measurement operators from linear dependencies
    a_red = [a_ops[x][:na - 1] for x in range(nx)]
    b_red = gen_reduced_meas_12(ny1,ny2,nb1,nb2,b_ops)

    # the AB level if needed
    AB = []
    if doAB:
        level = 1
        for op1 in flatten(a_red):
            for op2 in flatten(b_red):
                AB.append(op1 * op2)


    ### define substitution rules

    subs={**projective_measurement_constraints([a_red, b_red]),**get_orthog_subs(ny1,ny2,nb1,nb2,b_ops)}

    if local:
        subs={**subs,**get_local_subs(a_red,b_red)}

    norm_subs={**get_norm_subs_12(ny1,ny2,nb1,nb2,b_ops),**get_norm_subs(nx,na,a_ops)}

    seq_subs=get_seq_subs(ny1,ny2,nb1,nb2,b_ops)

    # define  momentequalities coming from sequential structure

    seq_constraints=get_seq_constraints(ny1,ny2,nb1,nb2,b_ops,b_red,a_red,subs,norm_subs,seq_subs,level,doAB)

    # add the equality constraints
    eq_constraints=[]
    for key in constraints:
        key_op = key.replace('A','a_ops')
        key_op = key_op.replace('B', 'b_ops')
        eq_constraints=eq_constraints+[apply_substitutions(eval(key_op), {**seq_subs,**norm_subs,**subs}) -  constraints[key]]

    #define the objective
    obj=0
    for key in coeffs:
        key_op = key.replace('A', 'a_ops')
        key_op = key_op.replace('B', 'b_ops')
        obj += coeffs[key] * apply_substitutions(eval(key_op), {**seq_subs,**norm_subs,**subs})

    if obj!=0:
        obj=1/2*(obj+obj.adjoint())
        obj=obj.expand()

    #set up the SDP relaxation

    sdp = SdpRelaxation(flatten([a_red, b_red]), verbose=1, normalized=True)

    generating_monomials = get_all_monomials(flatten([a_red, b_red]), [], subs, level) + AB

    sdp.get_relaxation(-1, extramonomials=generating_monomials,
                       substitutions=subs,
                       objective=obj,
                       momentequalities=seq_constraints+eq_constraints
                       )

    sdp.solve(solver=solver)

    return sdp


def q_seq_aabb(constraints,coeffs,scenario,level,local=False, solver='mosek'):

    # Measurement scenario
    nx1, nx2, ny1, ny2, na1, na2, nb1, nb2 = scenario

    if level=='AB':
        doAB=True
    else:
        doAB=False

    ### create operators
    alice = [[na1 for __ in range(nx1)], [na2 for __ in range(nx2)]]
    bob = [[nb1 for __ in range(ny1)], [nb2 for __ in range(ny2)]]

    #measurement operators
    a_ops = generate_measurements_12(alice, 'A_')
    b_ops = generate_measurements_12(bob, 'B_')

    #reduced measurement operators from linear dependencies
    a_red = gen_reduced_meas_12(nx1,nx2,na1,na2,a_ops)
    b_red = gen_reduced_meas_12(ny1,ny2,nb1,nb2,b_ops)

    # the AB level if needed
    AB = []
    if doAB:
        level = 1
        for op1 in flatten(a_red):
            for op2 in flatten(b_red):
                AB.append(op1 * op2)


    ### define substitution rules

    subs={**projective_measurement_constraints([a_red, b_red]),
          **get_orthog_subs(ny1,ny2,nb1,nb2,b_ops),
          **get_orthog_subs(nx1,nx2,na1,na2,a_ops)}

    if local:
        subs={**subs,**get_local_subs(a_red,b_red)}

    norm_subs={**get_norm_subs_12(ny1,ny2,nb1,nb2,b_ops),
               **get_norm_subs_12(nx1,nx2,na1,na2,a_ops)}

    seq_subs={**get_seq_subs(ny1,ny2,nb1,nb2,b_ops),
              **get_seq_subs(nx1,nx2,na1,na2,a_ops)}

    # define  momentequalities coming from sequential structure

    seq_constraints=get_seq_constraints(ny1,ny2,nb1,nb2,b_ops,b_red,a_red,subs,norm_subs,seq_subs,level,doAB)+\
                    get_seq_constraints(nx1,nx2,na1,na2,a_ops,a_red,b_red,subs,norm_subs,seq_subs,level,doAB)

    # add the equality constraints
    eq_constraints=[]
    for key in constraints:
        key_op = key.replace('A','a_ops')
        key_op = key_op.replace('B', 'b_ops')
        eq_constraints=eq_constraints+[apply_substitutions(eval(key_op), {**seq_subs,**norm_subs,**subs}) -  constraints[key]]

    #define the objective
    obj=0
    for key in coeffs:
        key_op = key.replace('A', 'a_ops')
        key_op = key_op.replace('B', 'b_ops')
        obj += coeffs[key] * apply_substitutions(eval(key_op), {**seq_subs,**norm_subs,**subs})

    if obj!=0:
        obj=1/2*(obj+obj.adjoint())
        obj=obj.expand()

    #set up the SDP relaxation

    sdp = SdpRelaxation(flatten([a_red, b_red]), verbose=1, normalized=True)

    generating_monomials = get_all_monomials(flatten([a_red, b_red]), [], subs, level) + AB

    sdp.get_relaxation(-1, extramonomials=generating_monomials,
                       substitutions=subs,
                       objective=obj,
                       momentequalities=seq_constraints+eq_constraints
                       )

    sdp.solve(solver=solver)

    return sdp

