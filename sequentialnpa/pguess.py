from .utils import *
from ncpol2sdpa import projective_measurement_constraints, SdpRelaxation

def pguess_abb(p,scenario,input,level,solver='mosek'):

    """
    Upper bound the local guessing probability of Bob in a 1 Alice, 2 Bob scenario
    :param p: dictionary containing the probabilities.
            e.g.
            p['A[x][a]*B[y1][y2][b1][b2]']=0.5 would indicate that p(a,b1,b2|x,y1,y2)=0.5

    :param scenario: list describing the measurement scenario
            format [nx,ny1,ny2,na,nb1,nb2] or [nx1,nx2,ny1,ny2,na1,na2,nb1,nb2]
            where nx, na is number of inputs/outputs for alice etc

    :param input: list containing the inputs for Bob for which to calculate the guessing prob
                    e.g. [0,1] calculates guessing prob for y1=0, y2=1
    :param level: int, relaxation level. 'AB' specifies level 1+AB
    :param solver: string, by default set to mosek
    :return: gp: float, bound on the guessing probability
            sdp: ncpol2sdpa.SdpRelaxation object (see ncpol2sdpa documentation)

    """

    nx,ny1,ny2,na,nb1,nb2 = scenario
    if level == 'AB':
        doAB=True
        level=1
    else:
        doAB=False

    ### create operators
    L1={}
    L2={}
    AB={}
    a_ops,b_ops,a_red,b_red,ab = gen_ops_pguess(scenario,doAB)

    ### generate subs and constraints
    subs={}
    norm_subs={}
    seq_subs={}
    seq_constraints=[]

    for i1 in range(nb1):
        for i2 in range(nb2):

            L1[i1, i2] = get_all_monomials(flatten([a_red[i1, i2], b_red[i1, i2]]), [], subs, 1)
            L2[i1, i2] = get_all_monomials(flatten([a_red[i1, i2], b_red[i1, i2]]), [], subs, 2)

            AB[i1,i2]=[]
            if doAB:
                for op1 in flatten(a_red[i1, i2]):
                    for op2 in flatten(b_red[i1, i2]):
                        AB[i1, i2].append(op1 * op2)

            subs = {**subs, **projective_measurement_constraints([a_red[i1, i2], b_red[i1, i2]])}

            subs = {**subs,**get_orthog_subs(ny1,ny2,nb1,nb2,b_ops[i1,i2])}

            norm_subs={**norm_subs, **get_norm_subs_12(ny1,ny2,nb1,nb2,b_ops[i1,i2]),
                       **get_norm_subs(nx,na,a_ops[i1,i2])}

            seq_subs={**seq_subs,**get_seq_subs(ny1,ny2,nb1,nb2,b_ops[i1,i2])}

            seq_constraints += get_seq_constraints(ny1, ny2, nb1, nb2, b_ops[i1,i2], b_red[i1,i2], a_red[i1,i2],
                                                  subs, norm_subs, seq_subs, level, doAB)

    # add the probability constraints
    eq_constraints = get_eq_constraints_pguess(p,a_ops,b_ops,nb1,nb2,L2)

    #   Ensure normalisation of the sum of moment matrices
    normstring = "0[0,0]"
    for i in range(1, nb1 * nb2):
        normstring = normstring + "+" + str(i) + "[0,0]"
    normstring = normstring + "-1.0"
    eq_constraints.append(normstring)

    # construct the guessing prob
    gp, extraexp = get_pguess(nb1,nb2,b_ops,input,seq_subs)

    ### prepare optimisation
    all_red_ops = [flatten([a_red[i, j], b_red[i, j]]) for i in range(nb1) for j in range(nb2)]
    extramons = {}
    for i1, i2 in product(range(nb1), range(nb2)):
        extramons[i1, i2] = flatten(AB[i1, i2])

    sdp = SdpRelaxation(all_red_ops, normalized=False, verbose=1)

    sdp.get_relaxation(level,
                       substitutions=subs,
                       objective=-gp,
                       momentequalities=eq_constraints,
                       extraobjexpr=extraexp,
                       extramonomials=list(extramons.values()))

    sdp.solve(solver="mosek")

    print('SDP status is ' + sdp.status)
    print('primal value ' + str(sdp.primal))
    print('dual value ' + str(sdp.dual))

    return sdp.dual, sdp




