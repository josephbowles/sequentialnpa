sequentialnpa
=========

This package allows one to perform optimisation over sets of sequential quantum correlations as found in https://arxiv.org/abs/1911.11056. 

The package contains two functions (see function definitions for more info)
- sequentialnpa: general optimisation over relaxations of Q_SEQ. For now, can handle two possible scenarios: one Alice, two Bobs or; two Alices, two Bobs. 
- pguess_abb: for upper bounding local guessing probabilities of Bob in a one alice, two Bob scenario (as in section 4.1 of article) 

dependencies
=========

The implementation requires ncpol2sdpa (https://gitlab.com/peterwittek/ncpol2sdpa) and related dependencies. 
An sdp solver is required (sdpa or mosek recommended). 

install
=========
via pip:

pip install git+https://gitlab.com/josephbowles/sequentialnpa.git

Contact: bowles.physics@gmail.com



